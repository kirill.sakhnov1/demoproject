import datetime
import json
import os
from typing import Any, Dict

import requests_async as requests
from aiocache import Cache
from flama.applications import Flama
from requests.exceptions import Timeout
from starlette.endpoints import HTTPEndpoint
from starlette.requests import Request
from starlette.responses import JSONResponse

from demosvc import logutil
from demosvc.rest.apimodel import Coords, Message, MessageSchema, PointInTime, PointInTimeSchema, Timestamp,\
    TimestampSchema

LOG = logutil.get_logger(__package__, __file__)

CACHABLE_STATUS_CODES = frozenset((200, 304))

cache = Cache()


def _return_pnt_int_time(dict_resp: Dict[str, Any]):
    """ Given the deserialized response body, returns formatted json. """
    pnt = PointInTime(
        source='vip-db',
        gps_coords=Coords(lat=dict_resp.get('latitude'), long=dict_resp.get('longitude'))
    )
    return JSONResponse(PointInTimeSchema().dump(pnt))


class _Timestamp(HTTPEndpoint):
    async def get(self, request: Request):
        """
        tags:
            - Demo
        description:
            Gets the current UNIX epoch timestamp in ISO 8601 format.
        responses:
            200:
                description: Timestamp.
                content:
                    application/json:
                        schema: TimestampSchema
        """
        timestamp_iso = datetime.datetime.now().isoformat()
        tms = Timestamp(f'{timestamp_iso}')
        return JSONResponse(TimestampSchema().dump(tms))


class _Coords(HTTPEndpoint):
    async def get(self, request: Request):
        """
        tags:
            - Demo
        description:
            Gets coordinates for point in time.
        parameters:
            - name: point_in_time
              in: path
              schema:
                type: integer
              required: true
              description: Point in time.
        responses:
            200:
                description: Serialized coordinates for point in time.
                content:
                    application/json:
                        schema: PointInTimeSchema
            400:
                description: HTTP Client Error - Bad request.
                content:
                    application/json:
                        schema: MessageSchema
            404:
                description: HTTP Client Error - Not found.
                content:
                    application/json:
                        schema: MessageSchema
            500:
                description: HTTP Server Error - Internal server error.
                content:
                    application/json:
                        schema: MessageSchema
            504:
                description: HTTP Server Error - Gateway timeout.
                content:
                    application/json:
                        schema: MessageSchema
        """
        point_in_time = request.path_params.get('point_in_time')
        cache_key = f'VIP/{point_in_time}'
        cached_resp = await cache.get(cache_key)

        if cached_resp:
            content = cached_resp.get('content')
            code = cached_resp.get('status_code')
            LOG.debug(f'{code} Returned from memory cache: {content}')
            return _return_pnt_int_time(content)

        db_endpoint_url = os.environ.get('DB_ENDPOINT_URL')
        endpoint_timeout = int(os.environ.get('ENDPOINT_TIMEOUT'))
        cache_ttl = int(os.environ.get('CACHE_TTL'))

        try:
            point_in_time = int(point_in_time)
        except ValueError as e:
            LOG.exception(e)
            return JSONResponse(MessageSchema().dump(Message(f'400 Bad request: invalid Integer:'
                                                             f' "{point_in_time}"')), 400)

        try:
            resp = await requests.request(method='GET',
                                          url=f"{db_endpoint_url}/{point_in_time}",
                                          timeout=endpoint_timeout)
            status_code = resp.status_code
            body = resp.text
            url = resp.url

            if 400 <= status_code < 500:
                LOG.debug(f'{status_code} Proxy Server error: {body} for url: {url}')
                return JSONResponse(MessageSchema().dump(Message(f'400 Client error: for url: {url}')),
                                    400)
            elif 500 <= resp.status_code < 600:
                LOG.debug(f'{status_code} Proxy Server error: {body} for url: {url}')
                return JSONResponse(MessageSchema().dump(Message(f'500 Server error: for url: {url}')),
                                    500)

            deserialized_resp = json.loads(body) if body else None
            if not deserialized_resp:
                LOG.debug(f'{status_code} Proxy Server error: {resp} for url: {url}')
                return JSONResponse(MessageSchema().dump(Message(f'404 Not found error: for url: {url}')),
                                    404)

            if status_code in CACHABLE_STATUS_CODES:
                resp_to_cache = {'content': resp.json(),
                                 'status_code': status_code}
                await cache.set(cache_key, resp_to_cache, ttl=cache_ttl)
                LOG.debug(f'Saved to memory cache: {resp_to_cache}')

            return _return_pnt_int_time(deserialized_resp)

        except Timeout as t:
            LOG.exception(t)
            msg = Message('504 Server error: request timeout')
            return JSONResponse(MessageSchema().dump(msg), 504)


def create(**kwargs):
    ns = Flama(**kwargs)
    ns.add_route('/now', _Timestamp)
    ns.add_route('/VIP/{point_in_time}', _Coords)
    return ns
