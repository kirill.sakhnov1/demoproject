import json
import os
import random
import re
import unittest
from unittest.mock import AsyncMock

import requests_async as requests

from parameterized import parameterized
from starlette.testclient import TestClient

from demosvc import app

# source: https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s07.html
ISO8601_REGEX = r'^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):' \
                r'([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?$'


def _is_iso_format(string: str):
    match_iso8601 = re.compile(ISO8601_REGEX).match
    try:
        if match_iso8601(string) is not None:
            return True
    except ValueError:
        pass
    return False


class _Response:
    def __init__(self, status_code: int = None, text: str = None, url: str = None):
        self.status_code = status_code
        self.text = text
        self.url = url

    def json(self):
        return json.loads(self.text)


class TestNsDemo(unittest.TestCase):
    def setUp(self) -> None:
        self.client = TestClient(app.create_app())
        self.db_endpoint_url = os.environ['DB_ENDPOINT_URL']
        self.endpoint_timeout = int(os.environ['ENDPOINT_TIMEOUT'])

    def test_get_time(self):
        response = self.client.get('/v1/now')
        self.assertEqual(200, response.status_code)
        timestamp = response.json()['now']
        self.assertTrue(_is_iso_format(timestamp))

    def test_get_point_in_time_invalid_arg_string(self):
        val = 'x'
        response = self.client.get(f'/v1/VIP/{val}')
        self.assertEqual(400, response.status_code)
        self.assertEqual(f'400 Bad request: invalid Integer: "{val}"', response.json()['message'])

    @parameterized.expand(
        ['/v1/VIP/', '/v1/', '/']
    )
    def test_get_invalid_endpoint(self, endpoint):
        response = self.client.get(endpoint)
        self.assertEqual(404, response.status_code)
        self.assertEqual('404 Not Found', f'{response.status_code} {response.text}')

    @parameterized.expand(
        list(zip([(random.randrange(400, 500)) for iter in range(5)]))
    )
    def test_client_error(self, er_code):
        requests.request = AsyncMock()
        point_in_time = 1
        requests.request.return_value = _Response(er_code, None, f'{self.db_endpoint_url}/{point_in_time}')
        response = self.client.get(f'v1/VIP/{point_in_time}')
        self.assertEqual(f'400 Client error: for url: {self.db_endpoint_url}/{point_in_time}',
                         response.json()['message'])

    @parameterized.expand(
        list(zip([(random.randrange(500, 600)) for _ in range(5)]))
    )
    def test_server_error(self, er_code):
        requests.request = AsyncMock()
        point_in_time = 1
        requests.request.return_value = _Response(er_code, None, f'{self.db_endpoint_url}/{point_in_time}')
        response = self.client.get(f'v1/VIP/{point_in_time}')
        self.assertEqual(f'500 Server error: for url: {self.db_endpoint_url}/{point_in_time}',
                         response.json()['message'])

    def test_not_found(self):
        requests.request = AsyncMock()
        point_in_time = 1
        requests.request.return_value = _Response(201, None, f'{self.db_endpoint_url}/{point_in_time}')
        response = self.client.get(f'v1/VIP/{point_in_time}')
        self.assertEqual(f'404 Not found error: for url: {self.db_endpoint_url}/{point_in_time}',
                         response.json()['message'])

    def test_get_point_in_time(self):
        requests.request = AsyncMock()
        point_in_time = 1
        expected_resp = {
            'source': 'vip-db',
            'gps_coords': {'lat': '0.1', 'long': '0.55'}
        }
        third_party_server_resp_body = {'latitude': '0.1', 'longitude': '0.55'}
        requests.request.return_value = _Response(202, json.dumps(third_party_server_resp_body),
                                                  f'{self.db_endpoint_url}/{point_in_time}')
        response = self.client.get(f'v1/VIP/{point_in_time}')
        self.assertDictEqual(expected_resp, response.json())
