# Demo RESTful API

Repository with code for simple Demo Web Service.

## Description

The REST service uses fast API framework and its hierarchy is like:

* **Uvicorn**: an ASGI server
  * **Starlette**: (uses Uvicorn) a web micro framework
    * **Flama**: (uses Starlette) an API micro framework with several additional features for building APIs 
    (e.g. Swagger UI, etc) [[1]]

## Setup and build

These are the steps for building the service from the clean repository clone.

```
# clone the repository
git clone ...
cd demoproject/

# prepare virtual environment
python3.8 -m venv 3.8.venv
source 3.8.venv/bin/activate
pip3 install -U wheel tox pip setuptools

cd svc
python3 setup.py clean
rm -Rf build dist .eggs src/*.egg-info
pip3 install -e .
python3 setup.py bdist_wheel
cd ..

# clean up the dist packages from previous tox runs
rm -Rf ~/.tox/distshare

# run tests
tox -c svc

# build demo service API image from docker file
docker build -t 'demoproject_service:test' svc

# run auxilary third-party API in container
docker-compose up

# poll staus
curl -X GET localhost:8088

# run demo service API in container
YOUR_IP=192.168.0.199  # change to your local IP address
docker run --rm -p 5000:5000 -e DB_ENDPOINT_URL='http://'${YOUR_IP}':8088/v1/coords' 'demoproject_service:test'

# or run in 3.8.venv (service will run in the DEBUG mode)
python3 -m demosvc.app --log-level DEBUG

# poll status
curl localhost:5000/status

# check 'demosvc' API interface in browser (via Swagger UI)
http://localhost:5000/docs/
```

The `setup.py` file defines real dependencies. It is used by both `tox` to run tests and by `docker`
to build final images.
Tox creates its own virtual environment and installs the package there (i.e. it doesn't run them in-place).
Similarly, Docker builds `python setup.py sdist_wheel` and then installs the result into the image with PIP 
(see `svc/Dockerfile`).

## References

[1]: https://flama.perdy.io/
