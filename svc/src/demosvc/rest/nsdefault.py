import pkg_resources

from flama.applications import Flama
from starlette.endpoints import HTTPEndpoint
from starlette.requests import Request
from starlette.responses import JSONResponse

from demosvc.rest.apimodel import Message, MessageSchema


class Status(HTTPEndpoint):
    async def get(self, request: Request):
        """
        tags:
            - Default
        description:
            Gets the service status.
        responses:
            200:
                description: Status message.
                content:
                    application/json:
                        schema: MessageSchema
        """
        version = pkg_resources.require('demosvc')[0].version
        msg = Message(f'demosvc {version} - Ready.')
        return JSONResponse(MessageSchema().dump(msg))


def create(**kwargs):
    ns = Flama(**kwargs)
    ns.add_route('/status', Status)
    return ns
