import logging
import logging.config
import os
import sys
from typing import Optional


def configure(*, log_file: str = None, log_level: str = None, log_stdout: bool = False):
    """ Configures logging framework. """
    # ROOT logger
    fmt = '%(asctime)s %(levelname)s [%(name)s]: %(message)s'
    datefmt = '%Y-%m-%d %H:%M:%S'
    root_level = log_level if log_level else logging.INFO
    if log_file:
        logging.basicConfig(format=fmt, datefmt=datefmt, level=root_level, filename=log_file)
    else:
        log_stream = sys.stdout if log_stdout else sys.stderr
        logging.basicConfig(format=fmt, datefmt=datefmt, level=root_level, stream=log_stream)

    # 'demoproject' logger
    logger = logging.getLogger('demoproject')
    # the default ROOT logger level is WARNING, but we want more detailed logging from 'demoproject' package
    logger.setLevel(logging.NOTSET if log_level else logging.DEBUG)
    logger.propagate = 1


def get_logger(package: Optional[str], file: Optional[str]) -> logging.Logger:
    """
    Gets a Logger object with its name set to the fully-qualified name of a python module. This is often the same
    as the module's `__name__` attribute, except when `__name__ == '__main__'`. It will return the root logger
    if neither `package` nor `file` are specified.

    Example: LOG = logutil.getLogger(__package__, __file__)

    @param package: module's package
    @param file: module's file path
    @return: `logging.Logger` instance
    """
    if package and file:
        return logging.getLogger('.'.join([package, os.path.splitext(os.path.basename(file))[0]]))
    elif file:
        return logging.getLogger(os.path.splitext(os.path.basename(file))[0])
    else:
        return logging.getLogger()
