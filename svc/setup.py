from setuptools import find_namespace_packages, setup

setup(
    name='demosvc',
    version='0.1.0',

    # Dependencies
    install_requires=[
        'requests-async>=0.6.2', 'starlette>=0.13.2,!=0.14.1', 'uvicorn>=0.1.13', 'flama>=0.13.3', 'elastic-apm~=5.8',
        'apispec~=3.3', 'PyYAML~=5.3', 'aiocache>=0.11.1',
    ],

    packages=find_namespace_packages(
        where='src',
        include=['demosvc', 'demosvc.*'],
        exclude=[]
    ),
    package_dir={'': 'src'},
    zip_safe=False,
    python_requires='>=3.8',
)
