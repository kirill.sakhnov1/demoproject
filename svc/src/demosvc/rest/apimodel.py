from dataclasses import dataclass

import marshmallow as m


@dataclass(frozen=True)
class Message:
    message: str


class MessageSchema(m.Schema):
    message = m.fields.String()

    @m.post_load
    def make_message(self, data, **kwargs):
        return Message(**data)


@dataclass(frozen=True)
class Timestamp:
    now: str


class TimestampSchema(m.Schema):
    now = m.fields.String()

    @m.post_load
    def make_timestamp(self, data, **kwargs):
        return Timestamp(**data)


@dataclass(frozen=True)
class Coords:
    lat: str
    long: str


class CoordsSchema(m.Schema):
    lat = m.fields.String()
    long = m.fields.String()

    @m.post_load
    def make_coordinates(self, data, **kwargs):
        return Coords(**data)


@dataclass(frozen=True)
class PointInTime:
    source: str
    gps_coords: Coords


class PointInTimeSchema(m.Schema):
    source = m.fields.String()
    gps_coords = m.fields.Nested(CoordsSchema)

    @m.post_load
    def make_timestamp(self, data, **kwargs):
        return PointInTime(source=data.get('source'), gps_coords=data.get('gps_coords'))
