import re
import unittest

from starlette.testclient import TestClient

from demosvc import app


class TestStatus(unittest.TestCase):
    def setUp(self) -> None:
        self.client = TestClient(app.create_app())

    def test_get_status(self):
        response = self.client.get('/status')
        self.assertEqual(200, response.status_code)
        self.assertTrue(re.fullmatch(r'demosvc \d+\.\d+\.\d+ - Ready\.', response.json()['message']))
