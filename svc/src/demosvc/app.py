"""
The simple RESTFul Demo Web Service.
"""
import argparse
import os

import pkg_resources
import uvicorn

from flama.applications import Flama

from demosvc import logutil
from demosvc.rest import nsdefault, nsdemo

LOG = logutil.get_logger(__package__, __file__)

DEFAULT_DB_ENDPOINT_URL = 'http://127.0.0.1:8088/v1/coords'
DEFAULT_ENDPOINT_TIMEOUT = 5  # seconds
DEFAULT_CACHE_TTL = 2 * 60    # seconds


def create_app(*, debug: bool = None) -> Flama:
    debug = debug if debug else False
    version = pkg_resources.require('demosvc')[0].version
    app = Flama(
        debug=debug,
        title='Demo REST API',
        version=version,
        description='This is the simple REST API for demonstration purpose.',
        schema='/schema/', docs='/docs/', redoc='/redocs/',
    )

    app.mount('/v1', app=nsdemo.create(debug=debug))
    app.mount('/', app=nsdefault.create(debug=debug))

    LOG.info(f'demosvc {version} - Ready.')

    if 'DB_ENDPOINT_URL' not in os.environ:
        LOG.info(f'Not found "DB_ENDPOINT_URL", default value is used "{DEFAULT_DB_ENDPOINT_URL}"')
        os.environ['DB_ENDPOINT_URL'] = DEFAULT_DB_ENDPOINT_URL

    if 'ENDPOINT_TIMEOUT' not in os.environ:
        LOG.info(f'Not found "ENDPOINT_TIMEOUT", default value is used "{DEFAULT_ENDPOINT_TIMEOUT}"')
        os.environ['ENDPOINT_TIMEOUT'] = str(DEFAULT_ENDPOINT_TIMEOUT)

    if 'CACHE_TTL' not in os.environ:
        LOG.info(f'Not found "CACHE_TTL", default value is used "{DEFAULT_CACHE_TTL}"')
        os.environ['CACHE_TTL'] = str(DEFAULT_CACHE_TTL)

    return app


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Starts Demo Web service.'
    )
    parser.add_argument('--host', dest='listen_host', default='0.0.0.0', metavar='STR',
                        help='Network interface to listen on.')
    parser.add_argument('--port', dest='listen_port', type=int, default=5000, metavar='INT', help='Port to listen on.')
    parser.add_argument('--debug', dest='debug', action='store_true',
                        help='Boolean indicating if debug tracebacks should be returned on errors.')

    group = parser.add_argument_group('Logging options')
    group.add_argument(
        '--log-file', dest='log_file', type=str, default=None, metavar='<path>',
        help='path to a log file that will be attached to the root logger')
    group.add_argument(
        '--log-level', dest='log_level', type=str, choices=['DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL'], default=None,
        help='logging level')
    group.add_argument(
        '--log-stdout', dest='log_stdout', action='store_true',
        help='send log messages to stdout if --log-file is not specified; otherwise log to stderr')

    args = parser.parse_args()
    logutil.configure(log_file=args.log_file, log_level=args.log_level, log_stdout=args.log_stdout)

    app = create_app(debug=args.debug)
    uvicorn.run(app, host=args.listen_host, port=args.listen_port)


if __name__ == '__main__':
    main()
else:
    logutil.configure()
    APP = create_app()
